﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public float dmg; // creats int for damage
   void OnCollisionEnter(Collision c)
    {
        TankAI ai = c.gameObject.GetComponent<TankAI>();// gets the AI's gameobject 
        if (ai != null)// if ai does not null 
        {
            ai.eHealth -= dmg;// take damage to enemy
            if (ai.eHealth <= 0)// is health goes to zero
            {
                Destroy(ai.gameObject);// kill enemy 
            }
        }
        Destroy(gameObject);// distorys the object 
    }
}
