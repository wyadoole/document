﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooting : MonoBehaviour
{
    public GameObject bullet;
    public Transform spl;
    public float force;
    public float fireRate;

    // Start is called before the first frame update
    void Start()
    {

    }
    public void Update()
    {
        shoot();  
    }

    // Update is called once per frame
    public void shoot()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            Debug.Log("player press shotting butto!!!");
            GameObject b = GameObject.Instantiate<GameObject>(bullet);
            GameObject.Destroy(b, 2f);
            Debug.Log("destoryed object");
            b.transform.position = spl.position;
            b.transform.rotation = spl.rotation;
            b.GetComponent<Rigidbody>().AddForce(spl.forward * force);
        }
    }
}
