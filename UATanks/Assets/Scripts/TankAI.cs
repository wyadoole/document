﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    public float fieldOfView=1.5f;
    public float playerDistance;// player distance -> enmey sight  
    public Transform spl;// spawn point for the bullet 
    public float force;// number of force added 
    public float eHealth; // enmey healht that can be changed - or +
    public GameObject bullet;//  creates a varaible that can be called using bullet gameobject 
    public float fireRate;// firerate for the enmey  
    private Transform tf;// enemys transofmr 
    private Transform playerTf;// player transform 
    private float fireTimer = 0f;// firetimer for 
    public float dmg =10f;// dmg damage that can be changed in the inspector 
    
    
    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();// gets the bullets tranform 
        playerTf = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();// gets teh players transform 
    }

    // Update is called once per frame
    void Update()
    {
        tf.LookAt(playerTf);// looks at player 
        fireTimer += Time.deltaTime;// start timeer
        if (fireTimer > fireRate)// decrease the firerate 
        {// start timer 
            enemyShooting();// call shooting function 
            fireTimer = 0;// end 
        }
        
        
    }

    void enemyShooting()
    {
        GameObject b = GameObject.Instantiate<GameObject>(bullet);// creates bullet
        GameObject.Destroy(b, 2f);// destorys bullet after 2 secound if it does not hit anything
        Debug.Log("destoryed object");// prints to console if bullet is destoryed 
        b.transform.position = spl.position;// gets position of bullet
        b.transform.rotation = spl.rotation;// gets rotation of bullet
        b.GetComponent<Rigidbody>().AddForce(spl.forward * force);// adds the force of bullet 
    }
}
