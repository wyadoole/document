﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tankMover : MonoBehaviour {
    public tankData data;// gets tankdata to be called in the script 
    public Transform tf;// gets player tranform 
    private float speed;// creats varible for player speed
    private float rotateSpeed = 40;// creates varaible for player turnspeed
    [HideInInspector] public GameObject prefab;// hides prefabe for player so that it can be accidentatly removed 
    
    // Use this for initialization
    public void Awake() {
        tf = gameObject.GetComponent<Transform>();// gets player transform as tf
    }
    void Start() {
        speed = gameObject.GetComponent<tankData>().moveSpeed;// calls player speed
        rotateSpeed = gameObject.GetComponent<tankData>().turnSpeed;// calls turnspeed for player

    }

    // Update is called once per frame
    void Update() {

   
        Move(speed);//calls move speed to be updated 
        Rotate(rotateSpeed);// calls rotate to be updated 
    }
    public void Move(float mSpeed) {// creats function for moveing the player 
        if (Input.GetKey(KeyCode.W))// creates input key for moving forward
            tf.position += tf.forward *mSpeed * Time.deltaTime;
        else if (Input.GetKey(KeyCode.S))// creates input key for moving backward 
            tf.position -= tf.forward * mSpeed * Time.deltaTime;

    }
    public void Rotate(float rSpeed){// creates fuction for rotating the player 
        if (Input.GetKey(KeyCode.D))// creates input button for turning right 
            tf.Rotate(Vector3.up,rSpeed * Time.deltaTime);
        else if (Input.GetKey(KeyCode.A))// creates input button for turning left 
            tf.Rotate(Vector3.up,-rSpeed * Time.deltaTime); 
    }
}