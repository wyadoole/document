﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayerCamera : MonoBehaviour
{
    public Transform PlayerTransform;// gets the players transform 
    private Vector3 _cameraOffset;// creates a camera offset 
    [Range(0.01f, 1.0f)]// creates range 
    public float SmoothFactor = 0.5f;// smoth factor so that the camera is not gittery 
    public bool LookatPLayer = true; // checks if it looks at the player 
    
    // Initailization of the follow camera to player
    void Start()
    {
        _cameraOffset = transform.position - PlayerTransform.position;// keeps the camera from being gittery 
    }

    // LateUpdate is Called after update Methods 
   /* void LateUpdate()
    {
        Vector3 newPos = PlayerTransform.position + _cameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

        if (LookatPLayer)
            transform.LookAt(PlayerTransform); 
    }*/
}
